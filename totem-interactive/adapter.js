window.addEventListener('load', function() {
    window.top.postMessage('contentLoaded', '*');

    var hashSpan = document.getElementById('interactionsHash'),
        leaderBoard = document.getElementById('leader-board'),
        landingPage = document.getElementById('landing-page'),
        getReady = document.getElementById('get-ready'),
        signup = document.getElementById('signup'),
        score = document.getElementById('score'),
        getReadyCounter = document.getElementById('counter'),
        topPlayerName = document.getElementById('top-player-name'),
        topPlayerScore = document.getElementById('top-player-score'),
        secondPlayerName = document.getElementById('second-player-name'),
        secondPlayerScore = document.getElementById('second-player-score'),
        thirdPlayerName = document.getElementById('third-player-name'),
        thirdPlayerScore = document.getElementById('third-player-score'),
        cntInterval, signupTimeout, signedUp = false,
        qrcode = new QRCode(document.getElementById("qrcode"), {
	    width : 200,
	    height : 200
        });

    function showLandingPage() {
        leaderBoard.style.display = 'none';
        getReady.style.display = 'none';
        signup.style.display = 'none';
        
        landingPage.style.display = 'block';
    }


    showLandingPage();

    function showLeaderBoard() {
        landingPage.style.display = 'none';
        getReady.style.display = 'none';
        signup.style.display = 'none';
        
        leaderBoard.style.display = 'block';
    }

    function showGetReady() {
        landingPage.style.display = 'none';
        leaderBoard.style.display = 'none';
        signup.style.display = 'none';
        
        getReady.style.display = 'block';
    }

    function showSignup() {
        landingPage.style.display = 'none';
        getReady.style.display = 'none';
        leaderBoard.style.display = 'none';
        
        signup.style.display = 'block';

        score.innerText = counter.text;
    }

    function showGame() {
        landingPage.style.display = 'none';
        getReady.style.display = 'none';
        signup.style.display = 'none';
        leaderBoard.style.display = 'none';
    }

    function fillTopPlayer(leader) {
        topPlayerName.innerText = leader.username;
        topPlayerScore.innerText = leader.score;
    }
    function fillSecondPlayer(leader) {
        secondPlayerName.innerText = leader.username;
        secondPlayerScore.innerText = leader.score;
    }
    function fillThirdPlayer(leader) {
        thirdPlayerName.innerText = leader.username;
        thirdPlayerScore.innerText = leader.score;
    }

    function fillLeaderBoard(leaders) {
        if (leaders.length == 0) {
            return;
        }
        
        fillTopPlayer(leaders[0]);
        fillSecondPlayer(leaders[1]);
        fillThirdPlayer(leaders[2]);
        var len = leaders.length;

        if (len > 7) {
            len = 7;
        }

        for (var i = 1; i <= len; i++) {
            var name = document.getElementById('leaders-name-' + i);
            var score = document.getElementById('leaders-score-' + i);
            var leaderWrapper = document.getElementById('leaders-' + (i-1)); 

            name.innerText = leaders[i-1].username;
            score.innerText = leaders[i-1].score;
            leaderWrapper.style.display = 'block';
        }
    }

    function restartGame() {
        window.top.postMessage('gameRestarted', '*');
        showLandingPage();
        signedUp = false;
        window.clearTimeout(signupTimeout);
        restart();
    }

    function startGame() {
        showGame();
        window.clearInterval(cntInterval);
    }

    window.addEventListener('game-over', function() {
        if (signedUp) {
            showLeaderBoard();
        }
        else {
            showSignup();
        }

        window.top.postMessage({score: counter.text, event: 'gameOver'}, '*');
        
        signupTimeout = setTimeout(function() {
            restartGame();
        }, 6000);
    });

    window.addEventListener('message', function(event) {
        //console.log(event, 'got event in adapter');

        if (typeof event.data !== 'object') {
            console.error('event data is not an object');
            return;
        }

        if (event.data.event === 'displayHash') {
            hashSpan.innerText = event.data.data.hash;
	    qrcode.makeCode(event.data.data.url + '?hash=' + event.data.data.hash);
        }
        else if (event.data.event === 'deviceConnected') {
            showGetReady();

            var cnt = 10;
            getReadyCounter.innerText = cnt;

            cntInterval = setInterval(function() {
                cnt--;
                getReadyCounter.innerText = cnt;

                if (cnt === 0) {
                    window.clearInterval(cntInterval);
                    restartGame();
                }
                
            }, 1000);
            
        }
        else if (event.data.event === 'deviceDisconnected') {
            restartGame();
        }
        else if (event.data.event === 'signup') {
            signedUp = true;
        }
        else if (event.data.event === 'leaderboard') {
            fillLeaderBoard(event.data.data);
        }
        else {
            if (event.data.event == 'click') {
                if (!started) {
                    startGame();
                }
                handleJumpStart();
            }
            else if (event.data.event == 'restart') {
                restartGame();
            }
        }
    }, false);
    
});

