'use strict';
var socket = io.connect('http://' + 'totem-core-interactions.stage.toteminteractive.io'); 
   
var tap_button = document.querySelector('.tap-place');
var restart_button = document.getElementById('restart');
var connect_button = document.querySelector('.connect');
var connect_popup = document.getElementById('connect-popup');
var connect_popup_error = document.getElementById('connect-popup-error');
var connect_error_button = document.getElementById('connect-error-button');
var error = document.querySelector('.error');
var hashSpan = document.getElementById('hash');
var signup = document.getElementById('signup');
var score = document.getElementById('game-result');
var signupButton = document.getElementById('signup-button');
var leaderBoard = document.getElementById('leader-board');
var signedUp = false;
var xhttp = new XMLHttpRequest();
var reconnectHash = "";


connect_error_button.addEventListener('touchstart', function() {
    document.location.href = document.location.origin+document.location.pathname; 
});



function signupWithNickName(cb) {
    var nickInput = document.getElementById("nick");
    if (nickInput.value == "" || nickInput.value == undefined) {
	alert("Type in your nick!");
	return;
    }
    window.localStorage.setItem("userNick", nickInput.value);

    if (cb) {
        cb(getNick());
    }
}

function getNick() {
    return window.localStorage.getItem("userNick");
}

error.innerText = 'disconnected';

var getQueryString = function ( field, url ) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};

var connect = function(hash) {    
    if(hash){
        init(socket, hash);
    } else {
        error.innerText = 'No hash provided. You should specify url param: "www.url.com?hash=12345"'; 
    }
};

var init = function(socket, hash){
    var emit = function(event, data, cb){
        if(socket){
           if(event === 'message'){                    
               if(data.data){
                   socket.emit(event, JSON.stringify(data));
               } else {
                   console.log('Incorrect message. It has to contain "data" property with object as a value, like {data : {}}', {event:event, data:data});
               }
           } else {
               socket.emit(event, data);
           }
            console.log('emitted: ' + event, data);
            if (cb) cb();
        } else {
            console.log('no socket available');
        }
    };

    var campaignId;

    var sendSignupEvent = function(nick) {
        if (signedUp) {
            emit('message', {event: 'signup', data: {username: nick}});
        }
        else {
            emit('message', {event: 'signup', data: {username: nick}},
                 function() {
                     showLeaderBoard(campaignId);
                 });
        }
        
    };

    var restartToHash = function(hash) {
        if (hash != "" && restart_button.disabled == false) 
            document.location.href = document.location.origin+document.location.pathname+'?hash='+hash;
    }

    signupButton.addEventListener("click", function(event) {
        if (signedUp) return;
	event.preventDefault();
	signupWithNickName(sendSignupEvent);
    });
    
    var deviceConnected = Rx.Observable.fromEvent(socket, 'device-connected'); 
    var connectionFailed = Rx.Observable.fromEvent(socket, 'device-connect-error');
    var connectionRejected = Rx.Observable.fromEvent(socket, 'session-rejected');
    var reconnectHashEvent = Rx.Observable.fromEvent(socket, 'reconnect-hash');
    var messages = Rx.Observable.fromEvent(socket, 'message');
    
    connectionFailed
        .take(1)
        .subscribe(
            function(x)  {
                error.innerText = x.err;
                connect_popup_error.style.display = 'block';
                socket.disconnect(); 
            });
            
    connectionRejected
        .take(1)
        .subscribe(
            function()  {
                error.innerText = 'Connections limit error'; 
                socket.disconnect(); 
            });

    reconnectHashEvent
        .take(1)
        .subscribe(
            function(data) {
                var dataJSON = JSON.parse(data);
                reconnectHash = dataJSON.data.hash;
                restart_button.disabled = false;
                restart_button.firstChild.data = "Try again";
            });
            
    var movement = new Rx.Subject();

    messages
        .subscribe(
            function(data) {
                try {
                    var obj = JSON.parse(data);

                    console.log(obj, 'received message');

                    campaignId = obj.data.campaignId;

                    if (obj.event === 'score') {
                        if (!signedUp) {
                            signup.style.display = 'block';
                            
                            score.innerText = obj.data.score;
                        } else {
                            showLeaderBoard(obj.data.campaignId);
                        }
                    }

                } catch(e) {
                    console.error(e, "Parsing message");
                }
            });
        
    var moves = movement
        .sample(100)
        .filter(function(x) {
            return x.rotationRate.alpha > 1; 
        });
        
    var clicks = Rx.Observable.fromEvent(tap_button,"touchstart");

    // window.ondevicemotion = function(event){
    //     movement.onNext(event);
    // };
    
    deviceConnected
        .take(1)
        .do(function(x) {
            console.log('device-connected', x);
            if (getNick()) {
                signedUp = true;
                sendSignupEvent(getNick());
            }
        })
        .concatMap(function(x)  {
            error.innerText = 'Connected';  
            return Rx.Observable.merge(moves, clicks)
                .takeUntil(connections);
        })
        .subscribe(
            function()  {                
                emit('message', {event:'click', data:[]});
            },
            function(e) {error.innerText = 'device connected error';});
            
    restarts
        .subscribe(
            function()  {
                restartToHash(reconnectHash);
                //emit('message', {event:'restart', data:[]});
            }, 
            function(e) {error.innerText = 'restart error';});

    
    
    emit('device-connect', {deviceId : getId(), hash : hash});    
};
 
var connections = Rx.Observable
    .fromEvent(connect_button, 'touchstart')
        .map(function(x) {return document.getElementById("hash").value;})
        .singleInstance();

var restarts = Rx.Observable
    .fromEvent(restart_button, 'touchstart')
        .singleInstance();


var connHash = getQueryString('hash');

if (connHash) {
    connect(connHash);
    // connect_button.style.display = 'none';
    // hashSpan.style.display = 'none';
    connect_popup.style.display='none';
}
else {
    connections
        .subscribe(
            function(hash)  {
                console.log('connect', hash);
                connect_popup.style.display='none';
                connect(hash);
            },
            function(e) {error.innerText = 'connections error';});    
}
    

    
function getId() {
    var id = localStorage.getItem('deviceId');
    if(!id){
        id = makeid();
        localStorage.setItem('deviceId', id);    
    }
    
    return id;
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function fillLeaderBoard(leaders) {
    var len = leaders.length;

    if (len > 7) {
        len = 7;
    }

    for (var i = 0; i < len; i++) {
        var name = document.getElementById('leader-' + i);
        var score = document.getElementById('leader-score-' + i);

        name.innerText = leaders[i].username;
        score.innerText = leaders[i].score;
    }

    leaderBoard.style.display = 'block';

    // setTimeout(function() {
    //     document.location.href = document.location.origin+document.location.pathname;
    // }, 6000);
}

xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
        try {
            var leaders = JSON.parse(xhttp.responseText);

            fillLeaderBoard(leaders);
            
        } catch(e) {
            console.error(e, "parsing http response");
        }
    }
};

function showLeaderBoard(campaignId) {
    xhttp.open("GET", "http://totem-reports.stage.toteminteractive.io/v1/events/leaderboards/campaigns/"+campaignId, true);
    
    xhttp.send();
}
